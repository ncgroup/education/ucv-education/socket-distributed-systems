import os
import sys
import log
import time
import json
import random
import op_file
import survey_functions
from termcolor import colored
from os.path import isfile, join
from utils import msg
import glossary
import pprint
import overlap_network
import request_response
import commands
from getpass import getpass
import bd_functions_answers


#
def control():

    while True:

        if glossary.LOGIN == None or glossary.PASSWORD == None:
            login_register()

        head("Menu Principal")

        print("")
        print(colored("0.- Pre-Inscripcion", "green"))
        print(colored("1.- Comenzar encuesta", "green"))
        print("")

        opcion = seleccione()

        if opcion == 0:
            menu_load_enable_preincripcion()
        if opcion == 1:
            menu_load_enable_fill()


#
def menu_load_enable_preincripcion():
    while True:

        head("Pre-Inscripciones disponibles")
        list_surveys_tmp = request_response.request(
            commands.COMMAND_LIST_ENABLE_SURVEY_FOR_PRE_INSCRIPCION, "")

        if list_surveys_tmp == None:
            os.system('clear')
            print(colored("No hay pre-inscripciones disponibles", "red"))
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)
            break

        list_surveys = []
        for file_detail in list_surveys_tmp:

            employee = {
                glossary.LOGIN_FIELD: glossary.LOGIN,
                glossary.KEY: -1
            }

            if file_detail.__contains__(glossary.EMPLEADOS) and employee in file_detail[glossary.EMPLEADOS]:
                continue
            list_surveys.append(file_detail)

        if len(list_surveys) == 0:
            os.system('clear')
            print(colored("No hay pre-inscripciones disponibles", "red"))
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)
            break

        else:

            print("")
            for file_detail in list_surveys:
                print(colored(str(list_surveys.index(file_detail)+1) + " - " +
                      file_detail["titulo"] + " - " + str(file_detail["status"][0].replace(glossary.STATUS_PREINSCRIPCION, "Periodo de pre-inscripcion ")), "blue"))
            print("")
            print(colored(glossary.SALIR_TEXT, "green"))
            print(colored("x.- Indique el numero de encuesta a pre-inscribirse", "green"))
            opcion = seleccione()

            if opcion < 0 or opcion > len(list_surveys):
                continue

            if opcion == 0:
                break

            v_titulo = list_surveys[opcion-1][glossary.TITULO]

            result = request_response.request(
                commands.COMMAND_PREINSCRIPCION, {
                    glossary.TITULO: v_titulo,
                    glossary.LOGIN_FIELD: glossary.LOGIN
                })

            if result == None:
                os.system('clear')
                print(colored("Ocurrio un problema en la actualizacion", "red"))
                print("")
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                break
            else:
                os.system('clear')
                print(colored("Usuario " + glossary.LOGIN +
                      " se agrego a la encuesta '" + v_titulo + "'", "blue"))
                print("")
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                break


#
def menu_load_enable_fill():
    while True:

        head("Encuestas para llenar")
        list_surveys = request_response.request(
            commands.COMMAND_LIST_ENABLE_SURVEY_FOR_FILL, {
                glossary.LOGIN_FIELD: glossary.LOGIN
            })

        if list_surveys == None or len(list_surveys) == 0:
            os.system('clear')
            print(colored("No hay encuestas disponibles", "red"))
            print("")
            input(glossary.PRESIONE_CUALQUIER_TECLA)
            break

        else:

            print("")
            for file_detail in list_surveys:
                print(colored(str(list_surveys.index(file_detail)+1) + " - " +
                      file_detail["titulo"] + " - " + str(file_detail["status"][0].replace(glossary.STATUS_PREINSCRIPCION, "Periodo de llenado ")), "blue"))
            print("")
            print(colored(glossary.SALIR_TEXT, "green"))
            print(colored("x.- Indique el numero de encuesta a comenzar", "green"))
            opcion = seleccione()

            if opcion < 0 or opcion > len(list_surveys):
                continue

            if opcion == 0:
                break

            v_titulo = list_surveys[opcion-1][glossary.TITULO]

            result = request_response.request(
                commands.COMMAND_START_FILL, {
                    glossary.TITULO: v_titulo,
                    glossary.LOGIN_FIELD: glossary.LOGIN
                })

            if result == None:
                os.system('clear')
                print(colored("Ocurrio un problema en la actualizacion", "red"))
                print("")
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                break
            else:
                os.system('clear')
                print(colored("Usuario " + result[glossary.LOGIN_FIELD] +
                      " se agrego a la encuesta '" + v_titulo + "' ,para acceder a ella use la clave: " + str(result[glossary.KEY]), "blue"))
                print("")
                print("")
                print(
                    colored("El sistema lo enviara para que comience a presentar la encuesta"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                fill_survey(v_titulo)
                break


#
def fill_survey(v_titulo):

    result = request_response.request(
        commands.COMMAND_SEARCH_SURVEY, {
            "title": v_titulo
        })

    if result == None:
        os.system('clear')
        print(colored("Ocurrio un problema al buscar la encuesta", "red"))
        print("")
        input(glossary.PRESIONE_CUALQUIER_TECLA)
        return

    random.shuffle(result["preguntas"])

    for question in result["preguntas"]:
        
        how_much_time_left = request_response.request(commands.COMMAND_HOW_MUCH_TIME_LEFT, {
            glossary.TITULO: v_titulo,
            glossary.LOGIN_FIELD: glossary.LOGIN
        })
        
        if how_much_time_left["minutes_left"]<=0:
            
            os.system('clear')
            print(colored("Vencio el tiempo para responder la encuesta","red"))
            input(glossary.PRESIONE_CUALQUIER_TECLA)
            return
        
        else:
        
            head("Cuestionario: " + v_titulo + "  ***(Quedan " + str(how_much_time_left["minutes_left"]) + " minutos para terminar la encuesta)")
            print("")
            print(colored(question, "blue"))
            print("")
            answer = ""

            while len(answer) == 0:
                answer = input(">")

            # Validar tiempo

            bd_functions_answers.add_update_answer({
                glossary.TITULO: v_titulo,
                glossary.LOGIN_FIELD: glossary.LOGIN,
                glossary.PREGUNTA: question,
                glossary.RESPUESTA: answer
            })

            print("")
            print(colored("La respuesta a: " + question + " fue " + answer, "blue"))
            time.sleep(int(result["tiempo_visualizacion_respuesta_de_pregunta"]))

    os.system('clear')
    print(colored("FELICIDADES CULMINO LA ENCUESTA: " + v_titulo, "blue"))
    input(glossary.PRESIONE_CUALQUIER_TECLA)


#
def login_register():

    while True:
        head("Acceso al sistema")

        print("")
        print(colored("0.- Ingresar", "green"))
        print(colored("1.- Registrarse", "green"))
        print("")

        opcion = seleccione()

        if opcion == 0:
            os.system('clear')
            print(colored("Acceso al sistema", "blue"))
            print(colored("-----------------", "blue"))
            print("")
            print(colored("Indique su Login", "blue"))
            glossary.LOGIN = input("")
            print("")
            print(colored("Password", "blue"))
            glossary.PASSWORD = getpass()
            result = request_response.request(commands.COMMAND_CHECK_LOGIN_IN, {
                glossary.LOGIN_FIELD: glossary.LOGIN,
                glossary.PASSWORD_FIELD: glossary.PASSWORD
            })

            if result == None:
                glossary.LOGIN = None
                glossary.PASSWORD = None
                print("")
                print(colored("Login/Password no concuerdan", "red"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                continue
            else:
                break

        if opcion == 1:
            os.system('clear')
            print(colored("Registrarse", "blue"))
            print(colored("-----------", "blue"))
            print("")
            login = input("Login: ")
            print("")
            nombre = input("Nombre: ")
            print("")
            email = input("Email: ")
            print("")
            telefono = input("Telefono: ")
            print("")
            departamento = input("Departamento: ")
            print("")
            print(colored("Password", "blue"))
            password = getpass()
            print(colored("Confirmar", "blue"))
            confirm = getpass()

            if not password == confirm:
                print("")
                print(colored("El password y la confirmacion no concuerdan", "red"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
                continue

            result = request_response.request(commands.COMMAND_SEARCH_EMPLOYEE, {
                glossary.LOGIN_FIELD: login
            })

            if not result == None:
                print("")
                print(colored("El usuario ya esta registrado", "red"))
                input(glossary.PRESIONE_CUALQUIER_TECLA)
            else:
                can_save_employee = request_response.request(commands.COMMAND_ADD_UPDATE_EMPLOYEE, {
                    glossary.LOGIN_FIELD: login,
                    glossary.EMAIL_FIELD: email,
                    glossary.NOMBRE_FIELD: nombre,
                    glossary.TELEFONO_FIELD: telefono,
                    glossary.DEPARTAMENTO_FIELD: departamento,
                    glossary.PASSWORD_FIELD: password
                })
                if can_save_employee == None:
                    print("")
                    print(colored("El usuario no pudo ser registrado", "red"))
                    input(glossary.PRESIONE_CUALQUIER_TECLA)
                else:
                    glossary.LOGIN = login
                    glossary.PASSWORD = password
                    break


#
def head(title):
    os.system('clear')

    while overlap_network.get_enable_server() == -1:
        print("Esperando por servidor")
        time.sleep(1)
    os.system('clear')

    print("")
    print(colored("Autor: Elbrick Salazar, Proyecto Sistemas Distribuidos", "blue"))
    print("")
    print("Usuario conectado: " + str(glossary.LOGIN))
    print("-------------------")
    print("")

    print("Directorio de trabajo actual: " + glossary.WORKDIR_APPLICATION)
    print("")
    print(colored(title, "green"))
    print(colored("----------------------------------------------", "green"))


#
def seleccione():
    opcion = input(glossary.SELECCIONE_UNA_OPCION)
    opcion_number = -1
    try:
        opcion_number = int(opcion)
    except Exception:
        opcion_number = -1
    return opcion_number


#
def log_show_error(error, e):
    log.debug(error + str(e))
    msg(error + str(e))


#
def print_servers():
    time.sleep(1)
    nodes = overlap_network.read_overlap_network_info()
    if not nodes == None:
        for node in nodes:
            print(node)
