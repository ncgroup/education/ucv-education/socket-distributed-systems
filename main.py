
import sys
import os 
import pathlib
#
rootDir = os.fspath(pathlib.Path(__file__).parent.resolve())
for path in pathlib.Path(rootDir).iterdir():
    if path.is_dir():
        sys.path.append(str(path))
#        

import glossary
if glossary.TYPE_APPLICATION==glossary.SERVER_DAEMON:
    import server_daemon
    server_daemon.run()
    
if glossary.TYPE_APPLICATION==glossary.CLIENT_ADMIN:    
    import client_admin
    client_admin.run()
    
if glossary.TYPE_APPLICATION==glossary.CLIENT_EMPLOYEE:    
    import client_employee
    client_employee.run()    
