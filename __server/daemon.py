import socket
import time
import json
import glossary
import log
import response_commands
import encrypt

from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
signal(SIGPIPE, SIG_DFL)


def run(data_node):
    print("Servidor up...")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    sock.bind(('', 0))
    sock.listen(1)

    # Para indicar a los clientes por cual puerto deben enviar la informacion
    data_node["hostname"] = socket.gethostname()
    data_node["address"] = socket.gethostbyname(socket.gethostname())
    data_node["type"] = glossary.TYPE_APPLICATION
    data_node["tcp_port_inbound"] = sock.getsockname()[1]

    log.info("Servidor up")
    log.info("   hostname: " + socket.gethostname())
    log.info("   ip      : " + socket.gethostbyname(socket.gethostname()))
    log.info("   port    : " + str(sock.getsockname()[1]))

    print("Servidor up")
    print("   hostname: " + socket.gethostname())
    print("   ip      : " + socket.gethostbyname(socket.gethostname()))
    print("   port    : " + str(sock.getsockname()[1]))

    sc, addr = sock.accept()
    data=""

    try:
        while True:
            

                token = sc.recv(glossary.BUFFER_SIZE)
                data = encrypt.decrypt_token(token)
                data = json.loads(data)
                print("Recibido " + str(data))

                response = json.dumps(response_commands.command_director(data))

                sc.send(encrypt.encrypt_message(response))
                print("Enviado..: " + str(response))

                sc, addr = sock.accept()

    except Exception as e:
        print("Server shutdown " + str(e) + " -- " + data)
        log.error("Server shutdown " + str(e) )
            
