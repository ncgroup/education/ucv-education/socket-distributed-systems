import json
import encrypt
import glossary
import survey_functions
import op_file
import bd_functions_survey
import bd_functions_employee
import commands
import utils


#
def command_director(data_json):

    #
    if data_json["command"] == commands.COMMAND_SEARCH_ALL_SURVEY:
        return bd_functions_survey.search_all()

    if data_json["command"] == commands.COMMAND_SEARCH_SURVEY:
        return bd_functions_survey.search_survey(data_json["data"]["title"])

    if data_json["command"] == commands.COMMAND_EXIST_SURVEY:
        return bd_functions_survey.exist_survey(data_json["data"]["title"])

    if data_json["command"] == commands.COMMAND_ADD_UPDATE_SURVEY:
        return bd_functions_survey.add_update_survey(data_json["data"])

    if data_json["command"] == commands.COMMAND_LIST_ENABLE_SURVEY_FOR_PRE_INSCRIPCION:
        return bd_functions_survey.list_enable_survey_for_pre_incripcion()

    if data_json["command"] == commands.COMMAND_PREINSCRIPCION:
        return bd_functions_survey.preinscripcion(data_json["data"])

    if data_json["command"] == commands.COMMAND_LIST_ENABLE_SURVEY_FOR_FILL:
        return bd_functions_survey.list_enable_survey_for_fill(data_json["data"])

    if data_json["command"] == commands.COMMAND_START_FILL:
        return bd_functions_survey.start_fill(data_json["data"])
    
    #
    if data_json["command"] == commands.COMMAND_DATETIME_SERVER:
        return  utils.dtm_to_str(utils.get_datetime)
    
    if data_json["command"] == commands.COMMAND_HOW_MUCH_TIME_LEFT:
        return  bd_functions_survey.how_much_time_left(data_json["data"])

    #
    if data_json["command"] == commands.COMMAND_SEARCH_ALL_EMPLOYEE:
        return bd_functions_employee.search_all()

    if data_json["command"] == commands.COMMAND_SEARCH_EMPLOYEE:
        return bd_functions_employee.search_employee(data_json["data"]["login"])

    if data_json["command"] == commands.COMMAND_EXIST_EMPLOYEE:
        return bd_functions_employee.exist_employee(data_json["data"]["login"])

    if data_json["command"] == commands.COMMAND_ADD_UPDATE_EMPLOYEE:
        return bd_functions_employee.add_update_employee(data_json["data"])

    if data_json["command"] == commands.COMMAND_CHECK_LOGIN_IN:
        return bd_functions_employee.check_login_in(data_json["data"])
