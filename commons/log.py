import logging
import datetime
import socket
import glossary
import utils
import os
from os.path import join

format_log_file="""%s/%s-%s.log"""
name_file = format_log_file % (glossary.LOG_APPLICATION,glossary.TYPE_APPLICATION,socket.gethostname())
full_path=join(glossary.LOG_APPLICATION, name_file)
if os.path.exists(full_path):
    os.remove(full_path)

logging.basicConfig(filename=name_file, encoding='utf-8', level=logging.DEBUG)

def debug(msg): 
    logging.debug(str(utils.get_datetime()) + "  " + msg)    
def info(msg):
    logging.info(str(utils.get_datetime()) + "  " + msg)    
def warning(msg):
    logging.warning(str(utils.get_datetime()) + "  " + msg)    
def error(msg):
    logging.error(str(utils.get_datetime()) + "  " + msg)                
    